from django.urls import path
from .views import api_sales, api_sale, api_salespersons, api_salesperson, api_customers, api_customer, api_unsold


urlpatterns = [
    path("sales/", api_sales, name="api_sales"),
    path("sales/<int:pk>/", api_sale, name="api_sale"),
    path("salespersons/", api_salespersons, name="api_salespersons"),
    path("salespersons/<int:pk>/", api_salesperson, name="api_saleperson"),
    path("customers/", api_customers, name="api_customers"),
    path("customers/<int:pk>/", api_customer, name="api_customer"),
    path("unsold/", api_unsold, name="api_unsold"),
]
