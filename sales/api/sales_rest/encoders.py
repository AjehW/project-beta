from common.json import ModelEncoder
from .models import Sales, AutomobileVO, Salesperson, Customer

class AutomobileVOEncoder(ModelEncoder):
    model = AutomobileVO
    properties = [
        "vin",
        "import_href",
        "id",
    ]
class SalespersonEncoder(ModelEncoder):
    model = Salesperson
    properties  = [
        "name",
        "employeenumber",
        "id",
    ]
class CustomerEncoder(ModelEncoder):
    model = Customer
    properties  = [
        "name",
        "address",
        "phone",
        "id"
    ]

class SalesEncoder(ModelEncoder):
    model = Sales
    properties = [
        "price",
        "employee",
        "customer",
        "automobile",
        "id",
    ]
    encoders = {
        "automobile": AutomobileVOEncoder(),
        "employee": SalespersonEncoder(),
        "customer": CustomerEncoder(),
    }
