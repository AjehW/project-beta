import { useEffect, useState } from 'react';
import { Link } from 'react-router-dom';

function SalespersonsList() {
  const [salespersons, setSalespersons] = useState([])

  const getData = async () => {
    const response = await fetch('http://localhost:8090/api/salespersons/');

    if (response.ok) {
      const data = await response.json();
      setSalespersons(data.salesperson)
    }
  }
  useEffect(() => {
    getData()
  }, [])

  const deleteSalesperson = async (id) => {
    const response = await fetch(`http://localhost:8090/api/salespersons/${id}/`, {
      method: 'DELETE',
      mode: "cors",
      headers: {
        "Content-Type": "application/json"
      }
    })
    const data = await response.json()
    setSalespersons(
      salespersons.filter((salesperson) => {
        return salesperson.id !== id;
      })
    )
  }

  return (
    <>
      <div className="d-grid gap-2 d-sm-flex justify-content-sm-center">
        <Link to="/salespersons/new" className="btn btn-primary btn-lg px-4 gap-3">Add a Salesperson</Link>
      </div>
      <table className="table table-striped">
        <thead>
          <tr>
            <th>Name</th>
            <th>EmployeeID</th>
            <th>Actions</th>
          </tr>
        </thead>
        <tbody>
          {salespersons.map(salesperson => {
            return (
              <tr key={salesperson.id}>
                <td>{salesperson.name}</td>
                <td>{salesperson.employeenumber}</td>
                <td>
                  <button onClick={() => deleteSalesperson(salesperson.id)}>Delete</button>
                </td>
              </tr>
            );
          })}
        </tbody>
      </table>
    </>
  );
}

export default SalespersonsList;
