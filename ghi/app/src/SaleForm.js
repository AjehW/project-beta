import React, { useState, useEffect } from 'react';

function SalesForm() {
  const [automobile, setAutomobile] = useState('')
  const [employee, setEmployee] = useState('')
  const [customer, setCustomer] = useState('')
  const [automobileList, setAutomobileList] = useState([]);
  const [employeeList, setEmployeeList] = useState([]);
  const [customerList, setCustomerList] = useState([]);
  const [price, setPrice] = useState('');

  const getData = async () => {
    const urlauto = 'http://localhost:8090/api/unsold/';
    const response = await fetch(urlauto);

    if (response.ok) {
      const data = await response.json();
      setAutomobileList(data.unsoldvins);
    }
  }

  const getCustomer = async () => {
    const urlcust = 'http://localhost:8090/api/customers/';
    const response = await fetch(urlcust);
    if (response.ok) {
      const data = await response.json();
      setCustomerList(data.customer);
    }
  }

  const getSalespersons = async () => {
    const urlemploy = 'http://localhost:8090/api/salespersons/';
    const response = await fetch(urlemploy);
    if (response.ok) {
      const data = await response.json();
      setEmployeeList(data.salesperson);
    }
  }

  useEffect(() => {
    getData();
    getCustomer();
    getSalespersons();
  }, []);


  const handleSubmit = async (event) => {
    event.preventDefault();

    const data = {};
    data.price = Number(price);
    data.employee = Number(employee);
    data.customer = Number(customer);
    data.automobile = automobile;



    const fetchConfig = {
      method: "POST",
      body: JSON.stringify(data),
      headers: {
        'Content-Type': 'application/json',
      },
    };
    const salesUrl = 'http://localhost:8090/api/sales/';
    const response = await fetch(salesUrl, fetchConfig);

    if (response.ok) {
      setAutomobile('');
      setEmployee('');
      setCustomer('');
      setPrice('');
      window.location.replace('/sales')
    }
    else {
      alert("FAILED, try again")
    }
  }

  return (
    <div className="row">
      <div className="offset-3 col-6">
        <div className="shadow p-4 mt-4">
          <h1>New Sale</h1>
          <form onSubmit={handleSubmit} id="create-sale-form">
            <div className="mb-3">
              <select onChange={(event) => setAutomobile(event.target.value)} required name="automobile" id="automobile" className="form-select">
                <option value="">Choose a automobile</option>
                {automobileList.map(auto => {
                  return (
                    <option key={auto.vin} value={auto.vin}>{auto.vin}</option>
                  )
                })}
              </select>
            </div>
            <div className="mb-3">
              <select onChange={(event) => setEmployee(event.target.value)} required name="employee" id="employee" className="form-select">
                <option value="">Choose an employee</option>
                {employeeList.map(employee => {
                  return (
                    <option key={employee.id} value={employee.id}>{employee.name}{employee.employeenumber}</option>
                  )
                })}
              </select>
            </div>
            <div className="mb-3">
              <select onChange={(event) => setCustomer(event.target.value)} required name="customer" id="customer" className="form-select">
                <option value="">Choose a Customer</option>
                {customerList.map(customer => {
                  return (
                    <option key={customer.id} value={customer.id}>{customer.name}</option>
                  )
                })}
              </select>
            </div>
            <div className="form-floating mb-3">
              <input onChange={(event) => setPrice(event.target.value)} placeholder="Price" required type="number" name="price" id="price" className="form-control" />
              <label htmlFor="price">Price</label>
            </div>
            <button className="btn btn-primary">Create</button>
          </form>
        </div>
      </div>
    </div>
  );
}

export default SalesForm;
