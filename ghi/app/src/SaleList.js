import { useEffect, useState } from 'react';
import { Link } from 'react-router-dom';

function SalesList() {
  const [sales, setSales] = useState([])
  const [employees, setEmployees] = useState([])
  const [employeeNumber, setEmployeeNumber] = useState("")

  const getData = async () => {
    const response = await fetch('http://localhost:8090/api/sales/');
    if (response.ok) {
      const data = await response.json();
      setSales(data.sales)
    }
  }

  const getSales = async () => {
    const response = await fetch('http://localhost:8090/api/salespersons/');
    if (response.ok) {
      const data = await response.json();
      setEmployees(data.salesperson)
    }
  }

  useEffect(() => {
    getData()
    getSales()
  }, [])

  const deleteSale = async (id) => {
    const response = await fetch(`http://localhost:8090/api/sales/${id}/`, {
      method: 'DELETE',
      mode: "cors",
      headers: {
        "Content-Type": "application/json"
      }
    })
    const data = await response.json()
    setSales(
      sales.filter((sale) => {
        return sale.id !== id;
      })
    )
  }

  const filterSales = employeeNumber ? sales.filter(sale => sale.employee.employeenumber == employeeNumber) : sales


  return (
    <>
      <div className="container">
        <select value={employeeNumber} onChange={e => setEmployeeNumber(e.target.value)}>
          <option value="">Choose an Employee</option>
          {employees.map(person => <option key={person.employeenumber} value={person.employeenumber}>{person.name}</option>)}
        </select>
      </div>
      <div className="d-grid gap-2 d-sm-flex justify-content-sm-center">
        <Link to="/sales/new" className="btn btn-primary btn-lg px-4 gap-3">Add a Sale</Link>
      </div>
      <table className="table table-striped">
        <thead>
          <tr>
            <th>Employee</th>
            <th>Employee ID</th>
            <th>Customer Name</th>
            <th>Vin #</th>
            <th>Price</th>
            <th>Actions</th>
          </tr>
        </thead>
        <tbody>
          {filterSales.map(sale => {
            return (
              <tr key={sale.id}>
                <td>{sale.employee.name}</td>
                <td>{sale.employee.employeenumber}</td>
                <td>{sale.customer.name}</td>
                <td>{sale.automobile.vin}</td>
                <td>{sale.price}</td>
                <td>
                  <button onClick={() => deleteSale(sale.id)}>Delete</button>
                </td>
              </tr>
            );
          })}
        </tbody>
      </table>
    </>
  );
}

export default SalesList;
