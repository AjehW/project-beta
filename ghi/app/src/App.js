import { BrowserRouter, Routes, Route } from 'react-router-dom';
import MainPage from './MainPage';
import ManufacturersList from './ManufacturerList';
import ManufacturerForm from './ManufacturerForm';
import ModelList from './ModelList';
import ModelForm from './ModelForm';
import AutomobileForm from './AutomobileForm';
import ServiceApptList from './ServiceApptList';
import ServiceForm from './ServiceForm';
import TechnicianForm from './TechnicianForm';
import AutomobilesList from './AutomobileList';
import CustomerList from './CustomerList';
import CustomerForm from './CustomerForm';
import SalespersonsList from './SalesPersonList';
import SalespersonsForm from './SalesPersonForm';
import SaleList from './SaleList';
import SaleForm from './SaleForm';

import Nav from './Nav';

function App() {
  return (
    <BrowserRouter>
      <Nav />
      <div className="container">
        <Routes>
          <Route path="/" element={<MainPage />} />
          <Route path="manufacturers" element={<ManufacturersList />} />
          <Route path="manufacturers">
            <Route path="new" element={<ManufacturerForm />} />
          </Route>
          <Route path="models" element={<ModelList />} />
          <Route path="models">
            <Route path="new" element={<ModelForm />} />
          </Route>
          <Route path="automobiles" element={<AutomobilesList />} />
          <Route path="automobiles">
            <Route path="new" element={<AutomobileForm />} />
          </Route>
          <Route path="sales" element={<SaleList />} />
          <Route path="sales">
            <Route path="new" element={<SaleForm />} />
          </Route>
          <Route path="customers" element={<CustomerList />} />
          <Route path="customers">
            <Route path="new" element={<CustomerForm />} />
          </Route>
          <Route path="salespersons" element={<SalespersonsList />} />
          <Route path="salespersons">
            <Route path="new" element={<SalespersonsForm />} />
          </Route>
          <Route path="services" element={< ServiceApptList />} />
          <Route path="services">
            <Route path="new" element={< ServiceForm />} />
          </Route>
          <Route path="technicians">
            <Route path="new" element={< TechnicianForm />} />
          </Route>
        </Routes>
      </div>
    </BrowserRouter>
  );
}

export default App;
