# CarCar

Team:

Ajeh Williams - Services
Dario Armendariz - Sales

## Design
Graphical UI design is basic bootstrap formats. It utilizes basic form so that we can create individual objects, as well as,  sets lists of created items in a simple color coded sequence table sequence. As for the microservices we set up. We create two individual microserves, one for sales and one for services. Each of these link to the automobile model in inventory through poller services. Inventory is our root model which contains information on the manufacturer, model, and automobile. React displays all the backened code through external connections for each set microservice.

## Service microservice
My AutomobileVO was created so that the VIN for the service request is compared to the VINs we have in our inventory in order to determine which cars will receive VIP treatment, the Technician model assigns a new technician and their employee id, and then Service model builds the components of a service request. I added automobile and technician as foreign keys to the Service model so that I could pull that data. I created a poller service to pull the Automobile information from our root Inventory microservice. My views add options to list, change, create, or delete technicians or services. My forms are set up to add technicians and service requests with all components needed. I connected the proper routes and urls to make seamless transitions from the Main page and other pages. Lastly, I separated the encoders to a new file and added the corresponding imports to reach them.


## Sales microservice

My models is composed of four different setups. First, I have my AutomobileVO which is a direct replication of Automobile from the inventory. To achieve this, I had to create a poller service that would connect to the inventory through the inner host(8000). From there I chose what information I would be pulling. I chose to limit to the Vin, ID number and a standard href. Second, I set up the base models for both Salesperson and Customer. This was just a simple model so that I have a format to get/create/put/delete the needed information within my views the set fields needed(name, employeenumber). Third, I repeated this process with Customer to include the information needed(name, address, phone). My last model was Sales in which it would create a "Sale" that would pull needed information from my other three models through a foreign key so that I may attach/utilize all the information into one process. On encoders.py, I created ecoders that decifer the information needed/utilized in my views. This was then imported into views to create my backened information so that I can set up my code properly.
