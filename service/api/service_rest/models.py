from django.db import models
from django.urls import reverse


class AutomobileVO(models.Model):
    vin = models.CharField(max_length=17, unique=True)
    import_href = models.CharField(max_length=200, unique=True)


class Technician(models.Model):
    name = models.CharField(max_length=200)
    employee_id = models.CharField(max_length=200)


class Service(models.Model):
    vin = models.CharField(max_length=200)
    customer_name = models.CharField(max_length=200)
    service_datetime = models.DateTimeField("%Y-%m-%d")
    reason = models.CharField(max_length=200,null=True)
    vip_status = models.CharField(max_length=10, null=True, default=False)
    completed_status = models.CharField(max_length=10, null=True, default=False)


    automobile = models.ForeignKey(
        AutomobileVO,
        related_name="services",
        on_delete=models.CASCADE,
        null=True,
        blank=True,
    )

    technician = models.ForeignKey(
        Technician,
        related_name="services",
        on_delete=models.PROTECT,
        null=True,
    )


    def get_api_url(self):
        return reverse("api_show_service", kwargs={"pk": self.pk})

    def __str__(self):
        return f"{self.vin} - {self.technician}"

    class Meta:
        ordering = ("vin", "technician")
