from common.json import ModelEncoder
from .models import Service, Technician


class TechnicianEncoder(ModelEncoder):
    model = Technician
    properties = [
        "name",
        "employee_id",
        "id",
    ]

class ServicesEncoder(ModelEncoder):
    model = Service
    properties = [
        "vin",
        "customer_name",
        "service_datetime",
        "technician",
        "reason",
        "vip_status",
        "completed_status",
    ]
    encoders = {
        "technician": TechnicianEncoder(),
    }
